var map = L.map('map', {
    zoom: 5,
    center: [55, 25]
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var colors = {
  'WIFI': '#a3be8c',
  'LTE': '#d08770',
  'BT': '#b48ead',
  'BLE': '#bf616a',
  'GSM': '#ebcb8b'
}

$.getJSON( "/data/data.json", function(data) {
    var items = [];
    $.each(data, function(key, value) {
        if (value.AccuracyMeters < 20)
            L.circle([value.CurrentLatitude, value.CurrentLongitude], {
                color: colors[value.Type],
                radius: value.AccuracyMeters
            }).addTo(map)
                .bindPopup("<b>Type: </b> " + value.Type +  "<br><b>SSID:</b> " + value.SSID +  "<br><b>MAC:</b> " + value.MAC);;
    });
});
